import React, { Component } from 'react';
import CustomersService from './customerService';

const customersService = new CustomersService();

class CustomerCreateUpdate extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        const { match: { params } } = this.props;
        if (params && params.pk) {
            customersService.getCustomer(params.pk).then((c) => {
                this.refs.firstName.value = c.clientNames;
                this.refs.lastName.value = c.clientLastNames;
                this.refs.email.value = c.identification;
                this.refs.phone.value = c.phoneNumber;
                this.refs.address.value = c.address;
            })
        }
    }

    handleCreate() {
        customersService.createCustomer(
            {
                "clientNames": this.refs.firstName.value,
                "clientLastNames": this.refs.lastName.value,
                "identification": this.refs.email.value,
                "phoneNumber": this.refs.phone.value,
                "address": this.refs.address.value,
            }
        ).then((result) => {
            alert("Customer created!");
        }).catch(() => {
            alert('There was an error! Please re-check your form.');
        });
    }
    handleUpdate(clientId) {
        customersService.updateCustomer(
            {
                "clientId": clientId,
                "clientNames": this.refs.firstName.value,
                "clientLastNames": this.refs.lastName.value,
                "identification": this.refs.email.value,
                "phoneNumber": this.refs.phone.value,
                "address": this.refs.address.value,
            }
        ).then((result) => {
            console.log(result);
            alert("Customer updated!");
        }).catch(() => {
            alert('There was an error! Please re-check your form.');
        });
    }
    handleSubmit(event) {
        const { match: { params } } = this.props;

        if (params && params.pk) {
            this.handleUpdate(params.pk);
        }
        else {
            this.handleCreate();
        }

        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label>
                        Nombres:</label>
                    <input className="form-control" type="text" ref='firstName' />

                    <label>
                        Apellidos:</label>
                    <input className="form-control" type="text" ref='lastName' />

                    <label>
                        Identificación:</label>
                    <input className="form-control" type="text" ref='phone' />

                    <label>
                        Teléfomo:</label>
                    <input className="form-control" type="text" ref='email' />

                    <label>
                        Dirección:</label>
                    <input className="form-control" type="text" ref='address' />



                    <input className="btn btn-primary" type="submit" value="Submit" />
                </div>
            </form>
        );
    }
}

export default CustomerCreateUpdate;