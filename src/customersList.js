import React, { Component } from 'react';
import CustomersService from './customerService';

const customer = new CustomersService();

class CustomersList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            customers: [],
            nextPageURL: ''
        };
        this.nextPage = this.nextPage.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        var self = this;
        customer.getCustomers().then(function (result) {
            console.log(result);
            self.setState({ customers: result.data, nextPageURL: result.nextlink })
        });
    }
    handleDelete(e, pk) {
        var self = this;
        customer.deleteCustomer({ pk: pk }).then(() => {
            var newArr = self.state.customers.filter(function (obj) {
                return obj.clientId !== pk;
            });

            self.setState({ customers: newArr })
        });
    }

    nextPage() {
        var self = this;
        console.log(this.state.nextPageURL);
        customer.getCustomersByURL(this.state.nextPageURL).then((result) => {
            self.setState({ customers: result.data, nextPageURL: result.nextlink })
        });
    }
    render() {

        return (
            <div className="customers--list">
                <table className="table">
                    <thead key="thead">
                        <tr>
                            <th>#</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Identificación</th>
                            <th>Teléfomo</th>
                            <th>Dirección</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.customers.map(c =>
                            <tr key={c.clientId}>
                                <td>{c.clientId}  </td>
                                <td>{c.clientNames}</td>
                                <td>{c.clientLastNames}</td>
                                <td>{c.identification}</td>
                                <td>{c.phoneNumber}</td>
                                <td>{c.addressclients}</td>
                                <td>
                                    <button onClick={(e) => this.handleDelete(e, c.pk)}> Delete</button>
                                    <a href={"/customer/" + c.pk}> Update</a>
                                </td>
                            </tr>)}
                    </tbody>
                </table>
                <button className="btn btn-primary" onClick={this.nextPage}>Next</button>
            </div>
        );
    }
}
export default CustomersList;